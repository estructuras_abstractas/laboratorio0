#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

##
# @brief Identifica el codón correspondiente al grupo de bases nitrogenadas.
# @param nitros String que contiene las bases nitrogenadas.
# @return El codón correspondiente al grupo de bases nitrogenadas. 
#
def regresar_codon ( nitros ):

    if nitros == "GCU" or nitros == "GCC" or nitros == "GCA" or nitros == "GCG" :
        return 'A'
    
    elif nitros == "CGU" or nitros == "CGC" or nitros == "CGA" or nitros == "CGG" or nitros == "AGA" or nitros == "AGG" :
        return 'R'

    elif nitros == "AAU" or nitros == "AAC" :
        return 'N'
    
    elif nitros == "GAU" or nitros == "GAC" :
        return 'D'
    
    elif nitros == "UGU" or nitros == "UGC" :
        return 'C'
    
    elif nitros == "CAA" or nitros == "CAG" :
        return 'Q'
    
    elif nitros == "GAA" or nitros == "GAG" :
        return 'E'
    
    elif nitros == "GGU" or nitros == "GGC" "" or nitros == "GGA" or nitros == "GGG" :
        return 'G'
    
    elif nitros == "CAU" or nitros == "CAC" :
        return 'H'
    
    elif nitros == "AUU" or nitros == "AUC" or nitros == "AUA" :    
        return 'I'
    
    elif nitros == "UUA" or nitros == "UUG" or nitros == "CUU" or nitros == "CUC" or nitros == "CUA" or nitros == "CUG" :
        return 'L'

    elif nitros == "AAA" or nitros == "AAG" :
        return 'K'
    
    elif nitros == "AUG" :
        return 'M'
    
    elif nitros == "UUU" or nitros == "UUC" :
        return 'F'
    
    elif nitros == "CCU" or nitros == "CCC" or nitros == "CCA" or nitros == "CCG" :
        return 'P'
    
    elif nitros == "UCU" or nitros == "UCC" or nitros == "UCA" or nitros == "UCG" or nitros == "AGU" or nitros == "AGC" :
        return 'S'

    elif nitros == "ACU" or nitros == "ACC" or nitros == "ACA" or nitros == "ACG" :
        return 'T'

    elif nitros == "UGG" :
        return 'W'
    
    elif nitros == "UAU" or nitros == "UAC" :
        return 'Y'
    
    elif nitros == "GUU" or nitros == "GUC" or nitros == "GUA" or nitros == "GUG" :
        return 'V'
    
    elif nitros == "UAA" or nitros == "UGA" or nitros == "UAG" :
        return '|'

    else:
        return 'x'


if __name__=="__main__":

    nitros = ""
    codones = ""

    ## Verifica si el usuario introdujo sólo una cadena de bases nitrogenadas.    
    if len ( sys.argv ) != 2:
        print ("\nEscriba sólo una cadena de carácteres, debe ser múltiplo de 3\n")
        exit(0)

    
    ## Verifica si la cadena introducida es múltiplo de 3.
    if len(sys.argv[1]) % 3 == 0:

        i = 0

        
        ## Bucle que convierte los caracteres introducidos de tipo char a string para 
        # facilitar su manipulacion, luego llama a la funcion regresar_codon y determina 
        # si devuelve un char diferente de x, donde x significa que hubo un error por parte
        # del usuario al introducir la cadena. 
    
        while i < len(sys.argv[1]):

            nitros = nitros + sys.argv[1][i]
            nitros = nitros + sys.argv[1][i+1]
            nitros = nitros + sys.argv[1][i+2]

            codones = codones + regresar_codon( nitros )

            if codones[-1] == 'x':
                print( "\nEsa base nitrogenada tiene un error -> ", nitros )
                exit(0)

            nitros = ""
            i = i+3

        if (codones[-1] == '|' or codones[0] == '|') and len(codones)>1:
            
            i = 1

            ## Determina si har codones de parada de más.
            while i < len(codones)-1:

                if codones[i] == '|':
                    print( "No puede haber codones de parada en medio de la cadena >:/" )
                    exit(0)
                
                i = i+1

            print (codones)

        else:
            print( "La cadena DEBE iniciar y terminar con un codon de parada >:(" )

    else:
        print( "\nVerifique la cadena porfa :/" )    