#include <iostream>
#include <string>
#include <fstream>

/** 
 * Parametros usados para control de errores, los nombres indican su funcion dentro del programa.
 */
#define OK 0
#define ERROR 1
#define PARAM_OK 2
#define CANTIDAD_BASES_NITRO 3
#define BASE_NITRO_NO_EXISTE 4
#define FALTA_UN_CODON_DE_PARADA 5
#define CODONES_DE_PARADA_EN_MEDIO 6

using namespace std;

/**
 * @brief Identifica el codón correspondiente al grupo de bases nitrogenadas.
 * @param nitros String que contiene las bases nitrogenadas.
 * @return El codón correspondiente al grupo de bases nitrogenadas. 
 */
char regresar_codon( string nitros )
{
    if( nitros == "GCU" || nitros == "GCC" || nitros == "GCA" || nitros == "GCG" ) 
        return 'A';
    
    else if( nitros == "CGU" || nitros == "CGC" || nitros == "CGA" || nitros == "CGG" || nitros == "AGA" || nitros == "AGG" )
        return 'R';

    else if( nitros == "AAU" || nitros == "AAC" )
        return 'N';
    
    else if( nitros == "GAU" || nitros == "GAC" )
        return 'D';
    
    else if( nitros == "UGU" || nitros == "UGC" )
        return 'C';
    
    else if( nitros == "CAA" || nitros == "CAG" )
        return 'Q';
    
    else if( nitros == "GAA" || nitros == "GAG" )
        return 'E';
    
    else if( nitros == "GGU" || nitros == "GGC" "" || nitros == "GGA" || nitros == "GGG" )
        return 'G';
    
    else if( nitros == "CAU" || nitros == "CAC" )
        return 'H';
    
    else if( nitros == "AUU" || nitros == "AUC" || nitros == "AUA" )    
        return 'I';
    
    else if( nitros == "UUA" || nitros == "UUG" || nitros == "CUU" || nitros == "CUC" || nitros == "CUA" || nitros == "CUG" )
        return 'L';

    else if( nitros == "AAA" || nitros == "AAG" )
        return 'K';
    
    else if( nitros == "AUG" )
        return 'M';
    
    else if( nitros == "UUU" || nitros == "UUC" )
        return 'F';
    
    else if( nitros == "CCU" || nitros == "CCC" || nitros == "CCA" || nitros == "CCG" )
        return 'P';
    
    else if( nitros == "UCU" || nitros == "UCC" || nitros == "UCA" || nitros == "UCG" || nitros == "AGU" || nitros == "AGC" )
        return 'S';

    else if( nitros == "ACU" || nitros == "ACC" || nitros == "ACA" || nitros == "ACG" )
        return 'T';

    else if( nitros == "UGG" )
        return 'W';
    
    else if( nitros == "UAU" || nitros == "UAC" )
        return 'Y';
    
    else if( nitros == "GUU" || nitros == "GUC" || nitros == "GUA" || nitros == "GUG" )
        return 'V';
    
    else if( nitros == "UAA" || nitros == "UGA" || nitros == "UAG" )
        return '|';

    else
        return 'x';
     
};

/**
 * @brief Programa que recibe por línea de comandos una cadena de bases nitrogenadas y devuelve los codones equivalentes.
 * @param argc Número de argumentos pasados por el usuario, incluido el nombre del archivo.
 * @param argv Contiene los argumentos pasados por el usuario.
 * @return Si la función terminó correctamente o si ocurrió un fallo en la ejecución.
 */
int main( int argc, char** argv )
{
    int contador_de_caracteres = 0;
    string nitros;
    string codones;

    /** 
     * Verifica los parametros introducidos por el usuario 
     */

    if( argc != PARAM_OK )
    {
        cout << endl << "Escriba sólo una cadena de carácteres, debe ser múltiplo de " << CANTIDAD_BASES_NITRO << endl << endl;
        return ERROR;
    }

    /**
     * Cuenta el numero de caracteres 
     */

    for( int i=0; argv[1][i]!='\0'; i++)
        contador_de_caracteres++;

    /** 
     * Verifica si la cadena es multiplo de 3 
     */

    if ( contador_de_caracteres % CANTIDAD_BASES_NITRO == 0 )
    { 
        /**
         * Bucle que convierte los caracteres introducidos de tipo char a string para 
         * facilitar su manipulacion, luego llama a la funcion regresar_codon y determina 
         * si devuelve un char diferente de x, donde x significa que hubo un error por parte
         * del usuario al introducir la cadena. 
         */
        for ( int i=0; i<contador_de_caracteres; i=i+3)
        {
            nitros.push_back( argv[1][i] );
            nitros.push_back( argv[1][i+1] );
            nitros.push_back( argv[1][i+2] );

            codones.push_back( regresar_codon( nitros ) );

            if( codones.back() == 'x') 
            {   
                cout << endl << "Esa base nitrogenada tiene un error -> " << nitros << endl << endl;
                return BASE_NITRO_NO_EXISTE;
            }

            nitros.clear();
        }

        /**
         * Verifica si se inicia y se termina con un codon de parada, ademas verifica que no hayan
         * mas codones de parada dentro de la cadena, si la cadena pasa los filtros 
         * se imprime en la terminal.
         */
        if(  (codones.front() == '|'  || codones.back() == '|') && codones.length() > 1 )
        {
            for( int i=1; i<codones.length()-1; ++i )
            {
                if( codones[i] == '|' ) 
                {
                    cout << endl << "No puede haber codones de parada en medio de la cadena >:/" << endl << endl;
                    return CODONES_DE_PARADA_EN_MEDIO;
                }    
            } 

            cout << endl << codones << endl << endl;
        }
        else
        {
            cout << endl << "La cadena DEBE iniciar y terminar con un codon de parada >:(" << endl << endl; 
            return FALTA_UN_CODON_DE_PARADA; 
        }
        
    }
    else
        cout << "\nVerifique la cadena porfa :/" << endl << endl;
    

    /** 
     * El programa finalizo correctamente 
     */

    return OK; 

}/* Fin de main */